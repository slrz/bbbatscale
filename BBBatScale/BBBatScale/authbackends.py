from django.contrib.auth.models import User, Group
import ldap
import ldap.dn as ldapdn
import ldap.filter as ldapfilter
from django.contrib.auth import get_user_model
from django.contrib.auth.backends import BaseBackend
import os


class LDAPBackend(BaseBackend):

    def authenticate(self, request, username=None, password=None):
        try:

            first_name, last_name, is_staff, is_teacher, email = self.get_ldap_account_infos(username, password)

            if not first_name:
                return None

            user = User.objects.get_or_create(username=username)[0]
            user.first_name = first_name
            user.last_name = last_name
            user.is_staff = is_staff
            user.is_superuser = is_staff
            if email:
                user.email = email
            user.save()

            group_teacher = Group.objects.get_or_create(name='Teacher')[0]
            if is_teacher:
                user.groups.add(group_teacher)
            else:
                user.groups.remove(group_teacher)

            return user
        except Exception:
            return None

    # Required for your backend to work properly
    def get_user(self, user_id):
        try:
            return User.objects.get(pk=user_id)
        except User.DoesNotExist:
            return None

    def get_ldap_account_infos(self, username, password):
        first_name = None
        last_name = None
        is_staff = False
        is_teacher = False
        email = None

        try:
            LDAP_SERVER = os.environ['LDAP_SERVER']
            LDAP_BASE = os.environ['LDAP_BASE']
            MEMBERSHIP_GRP = ['staff', 'faculty', 'bbb-moderators']
            ADMIN_GRP = ['its', 'bbb-admins']

            server = ldap.initialize(LDAP_SERVER)
            server.protocol_version = ldap.VERSION3
            server.set_option(ldap.OPT_REFERRALS, 0)
            server.simple_bind_s('uid=' + ldapdn.escape_dn_chars(username) + ',' + LDAP_BASE, password)
            criteria = '(&(objectClass=posixaccount)(uid=' + ldapfilter.escape_filter_chars(username) + '))'
            attributes = ['sn', 'givenName', 'memberOf', 'description', 'mail']
            result = server.search_ext_s(LDAP_BASE, ldap.SCOPE_SUBTREE, criteria, attributes)
            server.unbind()
            for dn, entry in result:
                first_name = entry['givenName'][0].decode("utf-8")
                last_name = entry['sn'][0].decode("utf-8")
                groups_available = entry['memberOf']
                email = entry['mail'][0].decode("utf-8")

                for group_required in MEMBERSHIP_GRP:
                    for group_member in groups_available:
                        group_member = group_member.decode('UTF-8')

                        if "cn={},".format(group_required) in group_member:
                            is_teacher = True

                for group_required in ADMIN_GRP:
                    for group_member in groups_available:
                        group_member = group_member.decode('UTF-8')

                        if "cn={},".format(group_required) in group_member:
                            is_staff = True
        except Exception:
            pass

        return first_name, last_name, is_staff, is_teacher, email


class LocalBackend(BaseBackend):
    def authenticate(self, request, username=None, password=None):
        user_model = get_user_model()
        try:
            user = user_model.objects.get(username=username)
        except user_model.DoesNotExist:
            return None
        else:
            if getattr(user, 'is_active', False) and user.check_password(password):
                return user
        return None

    def get_user(self, pk):
        try:
            return User.objects.get(pk=pk)
        except User.DoesNotExist:
            print('cant find user')
            return None

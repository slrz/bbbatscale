from core.models import GeneralParameter


def export_vars(request):
    gp = GeneralParameter.objects.first()
    return {'general_parameter': gp} if gp else {}

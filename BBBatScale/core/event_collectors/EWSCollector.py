from exchangelib import Credentials, Account, Configuration, DELEGATE, EWSDateTime
from core.event_collectors.base import EventCollectorStrategy
import uuid
from datetime import timedelta
import logging
from core.models import Room, RoomEvent
from django.conf import settings

logger = logging.getLogger(__name__)


class EWSCollector(EventCollectorStrategy):

    def __init__(self):
        self._necessary_keys = ["EWS_Server", "EWS_Room_Email", "EWS_User", "EWS_Password"]

    def collect_events(self, room_pk, parameters):
        logger.info("Start collect_events for EWS room with pk={}".format(room_pk))

        parameters = self._evaluate_parameters(room_pk, parameters, self._necessary_keys)
        credentials = Credentials(parameters["EWS_User"], parameters["EWS_Password"])
        config = Configuration(server=parameters["EWS_Server"], credentials=credentials)
        raccount = Account(primary_smtp_address=parameters["EWS_Room_Email"], config=config, autodiscover=False,
                           access_type=DELEGATE)
        room_account = [(raccount, 'Organizer', False)]

        start = raccount.default_timezone.localize(EWSDateTime.now())
        end = start + timedelta(hours=settings.EVENT_COLLECTION_SYNC_SYNC_HOURS)
        busy_info = next(raccount.protocol.get_free_busy_info(accounts=room_account, start=start, end=end))
        if busy_info.calendar_events is not None:
            events = busy_info.calendar_events
            room_events = self._create_room_events_from_ews_events(events, room_pk, raccount)

            self._update_room_events_in_db(room_events, room_pk)

    def _extract_current_event_for_ews_room(self, raccount):
        room_account = [(raccount, 'Organizer', False)]
        start = raccount.default_timezone.localize(EWSDateTime.now())
        end = start + timedelta(minutes=30)
        busy_info = next(raccount.protocol.get_free_busy_info(accounts=room_account, start=start, end=end))
        if busy_info.calendar_events is not None:
            events = busy_info.calendar_events
            return events[0]
        else:
            return None

    def _create_room_events_from_ews_events(self, ews_events, room_pk, raccount):
        return_events = {}
        room = Room.objects.get(pk=room_pk)

        for ews_event in ews_events:
            uid = uuid.uuid1()
            if ews_event.details is not None:
                name = ews_event.details.subject
            else:
                name = ews_event.busy_type
            start = raccount.default_timezone.localize(ews_event.start)
            end = raccount.default_timezone.localize(ews_event.end)

            room_event = RoomEvent(uid=uid, name=name, room=room, start=start, end=end)

            return_events[uid] = room_event

        return return_events

# Generated by Django 3.0.4 on 2020-03-18 14:05

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0014_jitsiinstance_cpu_usage'),
    ]

    operations = [
        migrations.AlterField(
            model_name='jitsiinstance',
            name='cpu_usage',
            field=models.FloatField(default=0.0),
        ),
    ]
